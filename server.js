var express = require('express');
var app = express();

var SERVERPORT = process.env.PORT || 3000;


var middleWare = require('./middleware.js');

app.use(middleWare.logger);

app.get('/about',middleWare.requireAuthentication,function(req,res){
    res.send('About Us !');
});

app.use(express.static(__dirname + "/public"));

app.listen(SERVERPORT,function(){
    console.log('Express server started at port no. : '+ SERVERPORT.toString() +'.');    
});

